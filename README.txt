
Ubercart Restricted Shipping
July 15th, 2010
created by: Deven Smith
uc_restricted_shipping

This module is called "Restricted Shipping" to allow a client to specify which states they are legally allowed to ship alcohol or other regulated products. Not only do certain states allow shipping of alcohol, but there are also a different maximum limit (qty) per state per length of time. This module allows users to specify where, how much, and how often for each specific state or province. This is similar to something in-place at amazon.com.

There are global limits that can be set per state, as well as product limits that will limit the number of a single product that can be sold to any one customer (good for "1 per customer").  The system will track orders made by a user and uses their previous orders to check against the time limits allowed.  It is recommended that anonymous checkout is turned off.